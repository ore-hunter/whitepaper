# 👨💻 Team

### Victor Guedez

Founder / Programmer

Twitter: [https://twitter.com/vicguedezio](https://twitter.com/vicguedezio)

LinkedIn: [https://www.linkedin.com/in/vicguedez/](https://www.linkedin.com/in/vicguedez/)

![](.gitbook/assets/victor.jpeg)

### Daniel Guedez

Founder / CEO

Twitch: [https://www.twitch.tv/mr\_dartg](https://www.twitch.tv/mr\_dartg)

Twitter: [https://twitter.com/MrDartg](https://twitter.com/MrDartg)

LinkedIn: [https://www.linkedin.com/in/daniel-guedez-06312122a](https://www.linkedin.com/in/daniel-guedez-06312122a)

![](<.gitbook/assets/WhatsApp Image 2021-12-21 at 19.03.33.jpeg>)

### Giovany Favela

Investor

LinkedIn: [https://www.linkedin.com/in/giovany-favela-b4920094](https://www.linkedin.com/in/giovany-favela-b4920094)

![](<.gitbook/assets/WhatsApp Image 2021-12-21 at 18.56.49.jpeg>)
