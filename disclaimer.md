# Disclaimer



By playing or participating in Ore Hunter directly or indirectly, I declare that; I identify myself as a player, accept and fully understand this Disclaimer.&#x20;

All players understand and agree that they are solely responsible for their investment decisions regarding the acquisition of speculative assets created by Ore Hunter.

&#x20;All players understand and accept the volatile and unpredictable nature of the crypto market, as well as the related known and unknown risks, including but not limited to:

**1. Uncertain Regulation and Legal Implications:**

&#x20;Laws and regulations related to the technology behind Ore Hunter are not fixed and are therefore subject to change, including those that could negatively impact the project and for which we may choose to cease operations.

**2. Changes to our policies:**&#x20;

The crypto gaming market is constantly evolving and projects like Ore Hunter need to adapt to new developments and trends on a regular basis.&#x20;

For this reason we reserve the right to add, amend, update, modify and decide on all policies related to Ore Hunter, including but not limited to the whitepaper of the project, smart contracts used in the blockchain, the game mechanics, the granting mechanics rewards and fees associated with participating in the game.

&#x20;All players agree that neither the developers nor their technology providers, partners, consultants or associated personnel are responsible for the effect that updates made to the project may have on the value of assets held by investors.

**3. Failure to Provide Complete Information:**&#x20;

Our whitepaper aims to provide the most up-to-date and important information related to Ore Hunter, its mechanics, granting of rewards, fees and related details, however it is impossible to provide all the necessary information, updated in real time, without this implying that it is not constantly updated. of the details contained in the whitepaper.

&#x20;The omission of important details at certain times during the project update is unavoidable and the player understands and accepts that the Ore Hunter team cannot and is not obliged to report in real time on every detail of the project.

**4. Behavior of the markets:**&#x20;

Blockchain is a highly competitive industry, all Ore Hunter players understand that their participation is subject to market forces that can positively or negatively impact the project beyond the inherence of the development team, its technology providers and advisers.&#x20;

All players understand and accept that the project could face technical difficulties, insolvency, changes in its mechanics and updates that could affect the development of Ore Hunter in the way it has been planned and cause unexpected effects on the value of assets in possession. of investors.

**5. Loss of human capital:**&#x20;

Our associates are Ore Hunter's most important resource. However, despite the efforts we make to retain our work team, there is the possibility that a member of the team decides to leave, something that is beyond the control of the project and that could indeterminately affect the value of the assets in investor ownership.

**6. Security Risks:**&#x20;

Digital assets are one of the most coveted targets for hackers, scammers and other criminal groups; many attempts to interfere with platforms like Ore Hunter have been reported in the past, including but not limited to: malware attacks, denial of service, phishing, and the like.

All players must be aware of the risk that these practices represent for their investment, in addition, players understand and accept that Ore Hunter may not be shielded against one hundred percent of the threats that impact the crypto asset market every day.

**7. Other unforeseen risks:**&#x20;

All players accept that there are still risks that have not been documented in this section and from which they must protect themselves through proper risk management.

&#x20;Neither Ore Hunter, its development team, technological providers, consultants, partners or associated personnel are responsible for direct or indirect material, economic, physical or psychological damages, derived from the participation of the players in Ore Hunter.
