# 💎 Ore Hunter

Ore Hunter was born as a farming game that has evolved into a company brand with reliable developers in the Blockchain world.

In this sense, we are a company with the objective of developing tools and services in all Blockchain networks, which can generate stable and continuous benefits over time for all partners. We are always focused on growth, and aim at being a bridge that brings everyone the opportunity to benefit from these technologies.

&#x20;If you want to be part of this family and grow with us, we invite you to join our DAO. You will be able to own shares that reward you a percentage of the profits generated by everything that is underway, and yet to come within Ore Hunter.&#x20;

Complete information on the DAO and its operation can be found [HERE.](https://dao-doc.orehunter.io/)

In this whitepaper you'll find listed all the functions already available, as well as those that are in development. More games, services and tools will be added as we move forward with the immediate objectives.
