# 👨👩👦 Reproducción

Para poder reproducir o combinar nuestros mineros, es necesario tener uno de cada genero.

El costo de realizar la reproducción es de 10$Usd en tokens mas una reducción de la resistencia para cada minero de -80.  Una minera tiene un máximo de 4 reproducciones.

La ventaja de reproducir nuestros mineros es que nos garantiza un mínimo de rareza para el nuevo minero dependiendo de los padres.

Ejemplo:&#x20;

Al unir un minero común + poco común nos garantiza mínimo un poco común.

Al unir un minero raro + poco común nos garantiza mínimo un raro.

### Tabla de ejemplos

{% tabs %}
{% tab title="Común" %}
| Minero A | Minero B   | Nuevo Minero |
| -------- | ---------- | ------------ |
| Común    | Común      | Común        |
| Común    | Poco Común | Poco Común   |
| Común    | Raro       | Poco Común   |
| Común    | Épico      | Raro         |
| Común    | Legendario | Raro         |
{% endtab %}

{% tab title="Poco Común" %}
| Minero A   | Minero B   | Nuevo Minero |
| ---------- | ---------- | ------------ |
| Poco Común | Común      | Poco Común   |
| Poco Común | Poco Común | Poco Común   |
| Poco Común | Raro       | Raro         |
| Poco Común | Épico      | Raro         |
| Poco Común | Legendario | Épico        |
{% endtab %}

{% tab title="Raro" %}
| Minero A | Minero B   | Nuevo Minero |
| -------- | ---------- | ------------ |
| Raro     | Común      | Poco Común   |
| Raro     | Poco Común | Raro         |
| Raro     | Raro       | Raro         |
| Raro     | Épico      | Épico        |
| Raro     | Legendario | Épico        |
{% endtab %}

{% tab title="Epico" %}
| Minero A | Minero B   | Nuevo Minero |
| -------- | ---------- | ------------ |
| Épico    | Común      | Raro         |
| Épico    | Poco Común | Raro         |
| Épico    | Raro       | Épico        |
| Épico    | Épico      | Épico        |
| Épico    | Legendario | Legendario   |
{% endtab %}

{% tab title="Legendario" %}
| Minero A   | Minero B   | Nuevo Minero |
| ---------- | ---------- | ------------ |
| Legendario | Común      | Raro         |
| Legendario | Poco Común | Épico        |
| Legendario | Raro       | Épico        |
| Legendario | Épico      | Legendario   |
| Legendario | Legendario | Legendario   |
{% endtab %}
{% endtabs %}

