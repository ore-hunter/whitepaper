---
description: Poderosos, leales y deseosos por ver el relucir de las menas preciosas.
---

# ⛏ Mineros

Son nuestros trabajadores, los cuales podemos obtener mediante la apertura de un cofre. Tenemos el 99% de probabilidad de conseguir un Minero y solo 1% de probabilidad de que sea una Minera.

Su costo será el equivalente de 20$Usd en tokens

#### Fuerza

Determina la cantidad de recurso que puede extraer el minero diariamente.

#### Resistencia

Determina la cantidad de recurso total que va a extraer el minero. Al llegar a cero se dará por finalizado el contrato con este minero. Recuerde guardar un poco de resistencia en un minero si desea reproducirlo en un futuro.

| Rareza     | Drop Chance | Fuerza | Resistencia |
| ---------- | ----------- | ------ | ----------- |
| Común      | 65%         | 16-27  | 550         |
| Poco Común | 20%         | 28-39  | 780         |
| Raro       | 10%         | 40-55  | 1100        |
| Épico      | 3.9%        | 56-79  | 1580        |
| Legendario | 1%          | 80-120 | 2400        |
| Hunter     | 0.1%        | 300    | 6000        |

##

![](<../../.gitbook/assets/Minero1 (2).jpg>)

![](../../.gitbook/assets/Minera2\_.jpg)

####
