---
description: Cuevas repletas de menas
---

# ⚒ Minas

Las minas, repartidas por toda la montaña colmillo podrán ser compradas en un antiguo cofre, que en su interior te dará un  titulo de propiedad. Solo después de comprar los cofres a los ancianos podrás descubrir tu suerte. Contrata mineros y extrae de tus minas hasta la ultima mena.&#x20;

Su costo será el equivalente de 20$Usd en tokens

| Rareza     | Drop Chance | Capacidad | Menas | USD Recompensa | Maquinaria |
| ---------- | ----------- | --------- | ----- | -------------- | ---------- |
| Común      | 82%         | 5         | 1500  | 90             | --         |
| Poco Común | 10%         | 8         | 2400  | 144            | 1          |
| Rara       | 5.9%        | 12        | 3600  | 216            | 2          |
| Épica      | 2%          | 15        | 4500  | 270            | 3          |
| Legendaria | 0.1%        | 20        | 6000  | 360            | 4          |

### Capacidad

Cantidad de mineros que pueden trabajar a la vez diariamente.

### Menas

Cantidad de recurso total que posee una mina. Estas se irán extrayendo hasta quedar totalmente vacía la mina.&#x20;



