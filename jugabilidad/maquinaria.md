---
description: Producción extra
---

# 🏭 Maquinaria

La maquinaria nos permitirá  tener una bonificación extra diaria que no afectara la resistencia de nuestros mineros. Mas producción, menos costos.

Su costo será el equivalente de 20$Usd en tokens

| Rareza     | Drop Chance | % EXTRA |
| ---------- | ----------- | ------- |
| Común      | 55%         | 10%     |
| Poco Común | 30%         | 15%     |
| Rara       | 10%         | 20%     |
| Épica      | 4%          | 25%     |
| Legendaria | 1%          | 35%     |
