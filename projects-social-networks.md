# 🌎 Project's social networks

### Official Web

[https://orehunter.io/](https://orehunter.io/)

### Project's social networks

Telegram: [https://t.me/OreHunterGame](https://t.me/OreHunterGame)

Discord: [https://discord.gg/ukghncxBxm](https://discord.gg/ukghncxBxm)

Twitter: [https://twitter.com/OreHunterGame](https://twitter.com/OreHunterGame)

YouTube: [https://www.youtube.com/channel/UCL7vWeiOPPLkIgc53KIn3Kw](https://www.youtube.com/channel/UCL7vWeiOPPLkIgc53KIn3Kw)
