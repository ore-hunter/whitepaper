# 🕹 Minigames

The mini games of the Ore Hunter Hub can bring you free fun, and if you manage to have a good skill and want to test yourself by participating in 1vs1 matches or events paid for entry tokens and win, you can get good rewards in Tokens.

We will list the first mini games that will be added to the Hub. They are traditional turn-based board games.

1. Rock, paper or scissors.
2. Square.
3. Tic Tac Toe
4. Checkers
5. Foxes and chickens

After these mini games we will continue to develop more always increasing the skill level required to win.
