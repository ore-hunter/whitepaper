---
description: Active since May 2022
---

# 🤖 Hub Ore Hunter



#### The Ore Hunter Hub is a tool that will be totally separate from farming/staking.Su moneda serán las Fichas, el cual se comprará in-game depositando BUSD.

The tokens will have a fixed cost of 0.1 BUSD, with a minimum purchase of 10 tokens at 1 BUSD. The liquidity fund of these tokens is created at the time of purchase, and all existing tokens will always have their backing in BUSD.

In the Hub you will find Minigames and Events in which you can participate for free or test yourself by participating in 1vs1 games or in events with many players paying a fee in tokens and taking many tokens as a reward when you WIN.

{% hint style="warning" %}
By earning tokens you can withdraw them at any time for BUSD and you will have a fixed withdrawal tax of 5%.&#x20;
{% endhint %}

#### Example:

A player has 100 chips to withdraw with an exit tax of 5%

#### 100 tokens multiplied by 0.1 = 10 BUSD

* The player will receive 9,5 BUSD in his wallet
* For development will be 0.5 BUSD

{% hint style="info" %}
All of these transfers are automatic with every withdrawal.
{% endhint %}
