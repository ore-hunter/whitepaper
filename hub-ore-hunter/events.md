# 🎉 Events

The events will be special rooms to carry out a competition between players of our mini-games or some other platform using the Hub as a tool to organize and deliver the prizes in Tokens.

Events are edited by their host at the time of creation.

You must define:

* A short description, or some special indication for the participants in case it is an event that takes place in a game or platform external to the Ore Hunter Hub.
* The number of maximum participants that the event can have are options already predefined in a maximum of 8,16,32,64,128,256,512.
* The cost of the fee in tokens to be able to participate, being able to be 0 tokens as a minimum and without a maximum limit.
* If the event is one of our minigames, you must select it.

Once the event is created, you can share the code so that the participants can enter.
