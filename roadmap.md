# 🗺 Roadmap

### Q4 2021

* [x] Game's economy simulator development.
* [x] Main web page deployment.
* [x] Economy simulator deployment

### Q1 2022

* [x] Pre-sale publicity period.
* [x] Pre-sale day.
* [x] Implementation of phase 1 in the BSC testnet
* [x] Alpha publishes phase 1 on the BSC testnet.
* [x] Phase 1 public beta on the BSC testnet.
* [x] Official release phase 1 of the game.(27/02/2022)

### Q2 2022

* [x] Arrival of the Ore Hunter Hub.
* [x] System to carry out events.(05/15/2022)
* [ ] Hub's first mini game games.
* [ ] First public sale of DAO shares/tickets(06/20/2022)

### Q3 2022

* [ ] Start of V2 Farming/Staking
* [ ] Development and publication of the following 4 mini games.

### Q4 2022

* [ ] Second public sale of DAO shares

### Q2 2023

* [ ] Third and last public sale of DAO shares

### To define:

* [ ] Standardization of systems to safely and efficiently add staff to the development team.
